# WordPress

__Theme__: Riiskit: https://github.com/Chrisriis/Riiskit

__Roles__: In most cases, the client/s are assigned to the _Editor_ role unless they really know what they're doing.

__Escaping__: Always escape your output by using the built-in functions: https://codex.wordpress.org/Validating_Sanitizing_and_Escaping_User_Data

A great way to securely output HTML through a WYSIWYG is to use: `apply_filters('the_content', $var);`

__Code Standards__: 
https://trello.com/b/0HxGziIO/utviklingsmetodikk-og-verkt-y

##Settings: 
__Permalinks__: Postname

__WP Retina 2x__: We use the picturefill method and we have bought a Pro license. Be sure to check these settings:

* Auto Generate
* Keep IMG SRC (Pro)
* Remember to add our serial code in the Pro tab (see the snippets).

__Wordfence__: These settings will naturally depend on the level of security needed, but here are some settings we often use:

* Lock out: 10/5
* Immediately block: admin, administrator
