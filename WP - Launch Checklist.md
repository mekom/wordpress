# WP - Launch Checklist

## General
* Make sure you have plugins for automatic backup, security, SEO and performance.
* Does it work with both _http_, _www_ and eventually _https_?
* Have you deactivated the maintenance mode/force-login?
* Have you deleted dummy posts, content, comments, users and media?
* Have you added content everywhere?
* Have you SEO optimised the posts and pages?
* Have you tested the userrole permissions?
* Does all images have "alt" text? Use empty (alt="") if it's just a decorative image.

### Settings
* __General:__ Email, timezone, dateformat and timeformat.
* __Reading:__ Number of posts and search engine visibility.
* __Discussion:__ Just see if everything is okay.
* __Media:__ See if the predefined sizes fit your needs.
* __Permalinks:__ You should in most cases use postname as your permalink structure.
* __WP Retina 2x:__ Turn off debug mode and turn on generate automatically.

## Code
* __PHP debug:__ Turn if off in the wp-config.php file when you're absolutely finished.
* __Check the console__ for JS errors.

## Plugins

* __Developer:__ Have you disabled the developer plugins and made sure there are no errors?
* __Forms:__ Have you used the correct email-addresses and tested them?